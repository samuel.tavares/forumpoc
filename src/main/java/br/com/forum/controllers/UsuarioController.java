package br.com.forum.controllers;

import br.com.forum.DTOs.UsuarioDTO;
import br.com.forum.dataProviders.ExcelExporter;
import br.com.forum.models.Usuario;
import br.com.forum.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @PostMapping()
    public Usuario criarUsuario(@RequestBody @Validated UsuarioDTO usuarioDTO){
        return usuarioService.criarUsuario(usuarioDTO.converter());
    }

    @GetMapping()
    public Iterable<Usuario> listarUsusarios(){
        return usuarioService.listarUsuarios();
    }

    @GetMapping("/download")
    public void exportarExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<Usuario> listaUsuarios = usuarioService.listarUsuarios();
        ExcelExporter excelExporter = new ExcelExporter(listaUsuarios);

        excelExporter.export(response);
    }
}
